import React from 'react';
import {apiURL} from "../../constants";

import messageImage from '../../assets/image/message.png';

const styles = {
    width: '100px',
    height: '100px',
    marginRight: '10px',
    float: 'left'
};

const PostThumbnail = (props) => {
    let image = messageImage;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    return <img src={image} style={styles} className="img-thumbnail" alt="message"/>;
};

export default PostThumbnail;
