import React, {Fragment} from 'react';
import {
    NavItem,
    NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <Fragment>
        <NavItem>

            <NavLink> Hello, {user.username}</NavLink>
        </NavItem>
        <NavItem>
            <NavLink tag={RouterNavLink} to="/add" exact>Add new post</NavLink>
        </NavItem>
        <NavItem>
            <NavLink onClick={logout}>Logout</NavLink>
        </NavItem>
    </Fragment>

);

export default UserMenu;