import {
    FETCH_COMMENT_SUCCESS,
    FETCH_POST_INFO_SUCCESS,
    FETCH_POSTS_SUCCESS
} from "../actions/PostActions";


const initialState = {
    posts: null,
    postInfo: {},
    comments: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_SUCCESS:
            return {...state, posts: action.data};
        case FETCH_POST_INFO_SUCCESS:
            return {...state, postInfo: action.data};
        case FETCH_COMMENT_SUCCESS:
            return {...state, comments: action.comment};
        default:
            return state;
    }
};

export default usersReducer;
