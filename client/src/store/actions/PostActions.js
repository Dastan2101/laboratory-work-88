import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';

export const FETCH_CREATE_POST_SUCCESS = 'FETCH_CREATE_POST_SUCCESS';
export const FETCH_POST_INFO_SUCCESS = 'FETCH_POST_INFO_SUCCESS';
export const FETCH_COMMENT_SUCCESS = 'FETCH_COMMENT_SUCCESS';

const fetchPostsSuccess = data => ({type: FETCH_POSTS_SUCCESS, data});
const createPostSuccess = () => ({type: FETCH_CREATE_POST_SUCCESS});
const fetchPostInfoSuccess = data => ({type: FETCH_POST_INFO_SUCCESS, data});
const fetchCommentsSuccess = comment => ({type: FETCH_COMMENT_SUCCESS, comment});

export const getPosts = () => {
    return dispatch => {
        return axios.get('/post').then(
            response => dispatch(fetchPostsSuccess(response.data))
        )
    }
};

export const createPost = (data) => {
    return (dispatch, getState) => {
        if (getState().users.user.token) {
            const token = getState().users.user.token;
            return axios.post('/post', data, {headers: {"Authorization": token}}).then(
                () => {
                    dispatch(createPostSuccess());
                    dispatch(push('/'))
                }
            );
        } else {
            dispatch(push('/login'))
        }
    }
};

export const getPostInfo = id => {
    return dispatch => {
        return axios.get('/post/' + id).then(
            response => dispatch(fetchPostInfoSuccess(response.data))
        )
    }
};

export const getComments = postId => {
    return dispatch => {
        return axios.get('/comment/' + postId).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};

export const postComment = (data, id) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        return axios.post('/comment/' + id, data, {headers: {"Authorization": token}}).then(
            () => getComments(id)
        )
    }
};