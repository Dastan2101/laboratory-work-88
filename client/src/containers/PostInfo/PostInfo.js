import React, {Component} from 'react';
import {getComments, getPostInfo, postComment} from "../../store/actions/PostActions";
import {connect} from "react-redux";
import {Alert, Button, CardBody, CardText, CardTitle, Input, InputGroup, InputGroupAddon, Row} from "reactstrap";
import PostThumbnail from "../../components/PostThumbNail/PostThumbNail";
import Col from "reactstrap/es/Col";

class PostInfo extends Component {

    state = {
        text: ''
    };

    inputChangeHandler = event => {
        this.setState({
            text: event.target.value
        })
    };


    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.getPostInfo(id);
        this.props.getComments(id);
    }

    createPost = () => {
        if (this.state.text !== '') {
            const data = {
                text: this.state.text
            };

            this.props.postComment(data, this.props.match.params.id);
        }


    };

    componentDidUpdate(prevProps) {
        if (prevProps.comments !== this.props.comments) {
            this.props.getComments(this.props.match.params.id);
        }
    }

    render() {

        let comments;
        let postComment;
        if (this.props.comments) {
            comments = this.props.comments.map(index => (
                <Alert color="success" key={index._id} style={{margin: '20px 0'}}>
                    <p style={{color: 'black'}}> author: {index.user.username}</p>
                    <p>{index.text}</p>
                </Alert>
            ))
        }

        if (this.props.user) {
            postComment = <div style={{marginTop: 30}}>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">text</InputGroupAddon>
                    <Input type="text" name="text" value={this.state.text} onChange={this.inputChangeHandler} required/>
                    <InputGroupAddon addonType="append">
                        <Button color="secondary"
                        onClick={this.createPost}>Comment</Button>
                    </InputGroupAddon>
                </InputGroup>
            </div>
        } else {
            postComment = <Alert color="danger">
                Sign in for comment this post
            </Alert>
        }

        return (
            <Row>
                <Col sm="12" md={{size: 6, offset: 3}}>
                    <PostThumbnail
                        image={this.props.postInfo.image}
                    />
                    <CardBody>
                        <CardTitle>{this.props.postInfo.title}</CardTitle>
                        <CardText>{this.props.postInfo.description}</CardText>
                        <CardText>
                            <small className="text-muted">{this.props.postInfo.datetime}</small>
                        </CardText>
                    </CardBody>
                </Col>
                <Col sm="12" md={{size: 6, offset: 3}}>
                    {postComment}
                </Col>
                <Col sm="12" md={{size: 6, offset: 3}}>
                    {comments}
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    postInfo: state.posts.postInfo,
    comments: state.posts.comments,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getPostInfo: id => dispatch(getPostInfo(id)),
    getComments: id => dispatch(getComments(id)),
    postComment: (data, id) => dispatch(postComment(data, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostInfo);