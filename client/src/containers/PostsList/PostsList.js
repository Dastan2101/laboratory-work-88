import React, {Component} from 'react';
import {getPosts} from "../../store/actions/PostActions";
import {connect} from "react-redux";
import {Card, CardBody, Spinner} from "reactstrap";
import {Link} from "react-router-dom";
import PostThumbnail from "../../components/PostThumbNail/PostThumbNail";

const style = {
    marginLeft: 10
};

class PostsList extends Component {
    componentDidMount() {
        this.props.getPosts();
    }

    render() {


        let posts;
        if (!this.props.posts) {
            return <Spinner/>
        } else {
            posts = this.props.posts.map(post => (
                <Card style={{marginBottom: '10px'}} key={post._id}>
                    <CardBody>
                        <PostThumbnail image={post.image}/>
                        <p>
                            <strong style={style}>
                                {'posted at: ' + post.datetime}
                            </strong>
                            <strong style={style}>
                                {' by: ' + post.user.username}
                            </strong>
                        </p>
                        <Link to={'/post/' + post._id} style={style}>
                            {post.title}
                        </Link>

                    </CardBody>
                </Card>
            ))
        }
        return (
            <div>
                {posts}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts
});

const mapDispatchToProps = dispatch => ({
    getPosts: () => dispatch(getPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);