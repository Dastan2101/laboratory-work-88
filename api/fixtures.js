const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Post = require('./models/Post');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'default user',
            password: '123',
            token: 'HERETOKEN'
        }
    );

    const post = await Post.create(
        {
            user: user._id,
            title: 'Some post text default',
            description: 'Some description text default',
            datetime: '20.05.2019 20:00'
        }
    );

    await Comment.create(
        {
            user: user._id,
            post: post._id,
            text: 'Some comment text from default user'
        }
    );


    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});