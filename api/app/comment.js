const express = require('express');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/:id', auth, async (req, res) => {

    let dataComment = req.body;

    dataComment.user = req.user._id;
    dataComment.post = req.params.id;

    const comment = new Comment(dataComment);

    comment.save()
        .then(() => res.send({message: "OK"}))
        .catch(error => res.sendStatus(error))
});

router.get('/:id', (req, res) => {
    Comment.find({post: req.params.id}).populate({path: 'user', select: 'username'})
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error))
});

module.exports = router;