const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const auth = require('../middleware/auth');

const Post = require('../models/Post');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [upload.single('image'), auth], (req, res) => {
    const postData = req.body;

    if (!postData.description && !postData.image) {
        res.send({message: "Field description or image must be full"})
    }

    if (req.file) {
        postData.image = req.file.filename;
    }

    postData.user = req.user._id;
    postData.datetime = new Date().toISOString();

    const post = new Post(postData);

    post.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error));

});

router.get('/', (req, res) => {
    Post.find().populate({ path: 'user', select: 'username' }).sort({datetime: -1})
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});

router.get('/:id', (req, res) => {
    Post.findById(req.params.id)
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500))
});


module.exports = router;