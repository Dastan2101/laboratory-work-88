const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/user');
const post = require('./app/post');
const comment = require('./app/comment');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/post', post);
    app.use('/comment', comment);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
